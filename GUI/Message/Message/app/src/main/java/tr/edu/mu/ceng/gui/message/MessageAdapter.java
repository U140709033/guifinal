package tr.edu.mu.ceng.gui.message;



import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageAdapter extends RecyclerView.ViewHolder implements View.OnClickListener {


        public TextView kimden,gelenMe;
        public ImageView bookImage,fav_image;

        private ItemClickListener itemClickListener;

        public MessageAdapter(@NonNull View itemView) {
            super(itemView);

            kimden =itemView.findViewById(R.id.kimden);
            gelenMe = itemView.findViewById(R.id.gelenMesaj);
            bookImage = itemView.findViewById(R.id.gelenimage);
            //fav_image = itemView.findViewById(R.id.fav);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition(),false);
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

}

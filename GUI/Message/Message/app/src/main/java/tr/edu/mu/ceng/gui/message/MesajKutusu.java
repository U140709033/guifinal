package tr.edu.mu.ceng.gui.message;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class MesajKutusu extends AppCompatActivity {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;

    FirebaseDatabase database;
    DatabaseReference messag;
    String categoryId = "";

    FirebaseRecyclerAdapter<Message, MessageAdapter> adapter;

    Database localDB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesaj_kutusu);


        database = FirebaseDatabase.getInstance();
        messag = database.getReference("Message");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_message);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        localDB = new Database(this);

        loadBookList();
    }


    private void loadBookList() {

        adapter = new FirebaseRecyclerAdapter<Message, MessageAdapter>(Message.class, R.layout.message_item, MessageAdapter.class, messag) {
            @Override
            protected void populateViewHolder(final MessageAdapter viewHolder, final Message model, final int position) {

                viewHolder.kimden.setText(model.getKimden());
                viewHolder.gelenMe.setText(model.getGelenMesaj());
                //Log.d("asdafgf",model.getGelenMesaj().toString());
                Picasso.with(getBaseContext()).load(model.getFotograf()).into(viewHolder.bookImage);


                final Message local = model;
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        Toast.makeText(view.getContext(), "" + local.getGelenMesaj(), Toast.LENGTH_SHORT).show();
                        //Intent intent = new Intent(view.getContext(), BookDetail.class);
                        //startActivity(intent);
                    }
                });
            }
        };

        recyclerView.setAdapter(adapter);

    }
}

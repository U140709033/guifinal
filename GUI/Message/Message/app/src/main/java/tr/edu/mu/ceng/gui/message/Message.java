package tr.edu.mu.ceng.gui.message;

public class Message {

        String gelenMesaj,kimden,fotograf;

    public Message(){

    }

    public Message(String gelenMesaj, String kimden, String fotograf) {
        this.gelenMesaj = gelenMesaj;
        this.kimden = kimden;
        this.fotograf = fotograf;
    }

    public String getGelenMesaj() {
        return gelenMesaj;
    }

    public void setGelenMesaj(String gelenMesaj) {
        this.gelenMesaj = gelenMesaj;
    }

    public String getKimden() {
        return kimden;
    }

    public void setKimden(String kimden) {
        this.kimden = kimden;
    }

    public String getFotograf() {
        return fotograf;
    }

    public void setFotograf(String fotograf) {
        this.fotograf = fotograf;
    }
}

package tr.edu.mu.ceng.gui.message;

import android.view.View;

public interface ItemClickListener {

    void onClick(View view,int position,boolean isLongClick);
}
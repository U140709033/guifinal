package com.example.esra.project;

public class User {

    private String UserId;
    private String NameSurname;
    private String Email;
    private String Password;

    public User(){

    }
    public String getUserId(){
        return UserId;
    }

    public void setUserId(String userId){
        this.UserId = userId;
    }

    public String getNameSurname() {
        return NameSurname;
    }

    public void setNameSurname(String nameSurname) {
        this.NameSurname = nameSurname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        this.Password = password;
    }

    public User(String nameSurname, String userId){
        this.UserId=userId;
        this.NameSurname=nameSurname;

    }
}

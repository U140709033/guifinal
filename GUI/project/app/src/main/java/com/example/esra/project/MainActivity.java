package com.example.esra.project;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity implements Home.OnFragmentInteractionListener,
        Profile.OnFragmentInteractionListener,Messages.OnFragmentInteractionListener,Settings.OnFragmentInteractionListener{

    Fragment currentFragment = null;
    FragmentTransaction ft;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ft = getSupportFragmentManager().beginTransaction();
        currentFragment = new Home();
        ft.replace(R.id.content, currentFragment);
        ft.commit();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    currentFragment = new Home();
                    switchFragment(currentFragment);
                    return true;
                case R.id.navigation_profile:
                    currentFragment = new Profile();
                    switchFragment(currentFragment);
                    return true;
                case R.id.navigation_mesagges:
                    currentFragment = new Messages();
                    switchFragment(currentFragment);
                    return true;
                case R.id.navigation_settings:
                    currentFragment = new Settings();
                    switchFragment(currentFragment);
                    return true;

            }
            return false;
        }
    };
    private void switchFragment(Fragment fragment) {
        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
